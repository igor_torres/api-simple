# API - CHAT BOTS

Implement an API for our Bots. This should consist of a HTTP/REST backend and a database.
The backend will have two resources: - /bots: the bot resource represent the bots registered in our
Bot platform. The /bots endpoint have to manage all operations related to bots i.e. create, read,
update and delete.
## POST /bots
### body
```json
{
"id": "36b9f842-ee97-11e8-9443-0242ac120002",
"name": "Aureo"
}
```
### Return codes
>201 = SUCESS
>400 = BAD REQUEST
>404 = Route not found
>500 = ERRO

## GET /bots/:id
### JSON
```json
{
"id": "36b9f842-ee97-11e8-9443-0242ac120002",
"name": "Aureo"
}
```
### Return codes
>200 = SUCESS
>400 = BAD REQUEST: invalid UUID
>404 = Route not found
>500 = ERRO
## POST /messages
### BODY
```json
{
"conversationId": "7665ada8-3448-4acd-a1b7-d688e68fe9a1",
"timestamp": "2018-11-16T23:30:52.6917722Z",
"from": "36b9f842-ee97-11e8-9443-0242ac120002",
"to": "16edd3b3-3f75-40df-af07-2a3813a79ce9",
"text": "Hello! How can I help you?"
}
```
### Return codes
>201 = SUCESSO: Message saved
>400 = BAD REQUEST: invalid UUID 
>404 = Route not found
>500 = ERRO

## GET /messages/:id
### JSON 
```json
{
"id": "16edd3b3-3f75-40df-af07-2a3813a79ce9",
"conversationId": "7665ada8-3448-4acd-a1b7-d688e68fe9a1",
"timestamp": "2018-11-16T23:30:52.6917722Z",
"from": "36b9f842-ee97-11e8-9443-0242ac120002",
"to": "16edd3b3-3f75-40df-af07-2a3813a79ce9",
"text": "Hello! How can I help you?"
}
```
### Return codes
>200 = SUCESS
>400 = BAD REQUEST: invalid UUID
>404 = Route not found
>500 = ERRO

## GET /messages?conversationId=:conversationId
### JSON
```json
[
{
"id": "16edd3b3-3f75-40df-af07-2a3813a79ce9",
"conversationId": "7665ada8-3448-4acd-a1b7-d688e68fe9a1",
"timestamp": "2018-11-16T23:30:52.6917722Z",
"from": "36b9f842-ee97-11e8-9443-0242ac120002",
"to": "16edd3b3-3f75-40df-af07-2a3813a79ce9",
"text": "Hello! How can I help you?"
},
{
"id": "67ade836-ea2e-4992-a7c2-f04b696dc9ff",
"conversationId": "7665ada8-3448-4acd-a1b7-d688e68fe9a1",
"timestamp": "2018-11-16T23:30:57.5926721Z",
"from": "16edd3b3-3f75-40df-af07-2a3813a79ce9",
"to": "36b9f842-ee97-11e8-9443-0242ac120002",
"text": "Do you wanna know the balance?"
}
]
```
### Return codes
>200 = SUCESSO: Consulta efetuada  com sucesso
>400 = BAD REQUEST: invalid UUID
>404 = Route not found
>500 = ERRO



### Criando Keyspace
```sql

CREATE KEYSPACE chat
  WITH REPLICATION = { 
   'class' : 'SimpleStrategy', 
   'replication_factor' : 1 
  };
```

### Criando Tabelas
**Table BOTS**
```sql
CREATE TABLE chat.bots (
   id uuid, 
   name text,  
   PRIMARY KEY (id));
```
**Table MENSSAGE_BY_ID**
```sql
CREATE TABLE chat.message_by_id(
   id uuid, 
   conversationId uuid, 
   timestampMsg timestamp,
   fromId uuid,
   toId uuid, 
   textMsg text,
   PRIMARY KEY ((id),conversationId));
```

**Table MESSAGE BY ID CONVERSATION**
```sql
CREATE TABLE chat.message_by_idconversation(
   conversationId uuid, 
   timestampMsg timestamp,
   fromId uuid,
   toId uuid, 
   textMsg text,
   PRIMARY KEY ((conversationId),toId));
```
